﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Family_Generator
{

	public enum PersonState
	{
		Alive,
		Dead
	}

	public enum MarriageStatus
	{
		Single,
		Married,
		Widowed
	}

	public enum CauseOfDeath
	{
		None,
		Decay,
		Murder,
		Illness,
		Battle,
		Accident,
		ChildDeath
	}

	public enum SettingFamilyChange
	{
		Female,
		Male,
		Both
	}

	public enum SettingInheritThrone
	{
		OnlyMale,
		OnlyFemale,
		Both
	}

	[JsonObject (MemberSerialization.OptIn)]
	public class GeneratorState
	{

		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public SettingFamilyChange settingFamilyChange;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public SettingInheritThrone settingInheritThrone;

		[JsonProperty]
		public int currentYear;
		[JsonProperty (ItemConverterType = typeof (JsonRefedConverter))]
		public List<Family> families = new List<Family> ();

		private List<Family> _markedForDeletion = new List<Family> ();

		public void CheckForEmptyFamilies ()
		{
			foreach (Family fam in families)
			{
				if (fam.persons.Count == 0)
				{
					//Trace.WriteLine (currentYear + " -> " + fam.surname + " is now empty and removed!");
					MarkForDeletion (fam);
				}
			}
		}

		public void MarkForDeletion (Family f)
		{
			if (_markedForDeletion == null)
				_markedForDeletion = new List<Family> ();

			_markedForDeletion.Add (f);
		}

		public void CleanUp ()
		{
			if (_markedForDeletion != null && _markedForDeletion.Count > 0)
			{
				foreach (Family fam in _markedForDeletion)
				{
					if (fam != null)
						families.Remove (fam);
				}

				_markedForDeletion.Clear ();
			}
		}
	}

	[JsonObject (MemberSerialization.OptIn)]
	public class Family : IJsonLinkable
	{
		[JsonProperty]
		public string surname;
		[JsonProperty]
		public bool isFamilyDead;
		[JsonProperty (ItemConverterType = typeof (JsonRefedConverter))]
		public List<Person> persons = new List<Person> ();

		private List<Person> _markedForDeletion = new List<Person> ();

		public Family (string surname)
		{
			this.surname = surname;
		}

		public Family ()
		{

		}

		public override string ToString ()
		{
			return surname;
		}

		public void MarkForDeletion (Person p)
		{
			if (_markedForDeletion == null)
				_markedForDeletion = new List<Person> ();

			_markedForDeletion.Add (p);
		}

		public void CleanUp ()
		{
			if (_markedForDeletion != null && _markedForDeletion.Count > 0)
			{
				foreach (Person p in _markedForDeletion)
				{
					if (p != null)
						persons.Remove (p);
				}

				_markedForDeletion.Clear ();
			}

			foreach (Person p in persons)
			{
				if (p.state == PersonState.Alive)
				{
					isFamilyDead = false;
					return;
				}
			}

			isFamilyDead = true;
		}

		string IJsonLinkable.Id { get { return surname; } }
	}

	[JsonObject (MemberSerialization.OptIn)]
	public class Person : IJsonLinkable
	{
		public const int MARRIAGEAGE = 15;

		[JsonProperty]
		public string name;
		[JsonProperty, JsonConverter (typeof (JsonRefConverter))]
		public Family family;
		[JsonProperty]
		public string maidenName;
		[JsonProperty]
		public int age;
		[JsonProperty]
		public bool isFemale;
		[JsonProperty]
		public bool isKing;

		[JsonProperty]
		public Chance chanceToDie;
		[JsonProperty]
		public Chance chanceToMarry;
		[JsonProperty]
		public Chance chanceToPregnancy;
		[JsonProperty]
		public Chance chanceToSwitchFamily;

		[JsonProperty]
		public PersonalityTrait personality;
		[JsonProperty]
		public EyeColorTrait eyeColor;
		[JsonProperty]
		public HairColorTrait hairColor;

		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public PersonState state;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public MarriageStatus marriage;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public CauseOfDeath causeOfDeath;

		[JsonProperty, JsonConverter (typeof (JsonRefConverter))]
		public Person father;
		[JsonProperty, JsonConverter (typeof (JsonRefConverter))]
		public Person mother;

		[JsonProperty (ItemConverterType = typeof (JsonRefConverter))]
		public List<Person> spouses = new List<Person> ();
		[JsonProperty (ItemConverterType = typeof (JsonRefConverter))]
		public List<Person> children = new List<Person> ();

		[JsonProperty]
		public int yearOfBirth;
		[JsonProperty]
		public List<int> yearsOfMarriage = new List<int> ();
		[JsonProperty]
		public int yearOfFirstChildbirth;
		[JsonProperty]
		public int yearOfLastChildbirth;
		[JsonProperty]
		public int yearOfDeath;
		[JsonProperty]
		public int yearOfCrown;

		public Person CurrentSpouse
		{
			get
			{
				if (spouses != null && spouses.Count > 0)
					return spouses[spouses.Count - 1];

				return null;
			}
		}

		public Person (Family family, string name, int birthYear, int currentYear = 0)
		{
			this.family = family;
			this.name = name;
			yearOfBirth = birthYear;
			age = currentYear - birthYear;
			state = PersonState.Alive;
			marriage = MarriageStatus.Single;
			chanceToDie = new Chance (0, 20);
			chanceToSwitchFamily = new Chance (0, 50);

			InheritTraits ();
		}

		public Person ()
		{

		}

		public void InheritTraits ()
		{
			if (personality == null)
				personality = new PersonalityTrait ();
			if (eyeColor == null)
				eyeColor = new EyeColorTrait ();
			if (hairColor == null)
				hairColor = new HairColorTrait ();

			if (father == null || mother == null)
			{
				personality.Create ();
				eyeColor.Create ();
				hairColor.Create ();
			}
			else
			{
				personality.Inherit (father.personality, mother.personality);
				eyeColor.Inherit (father.eyeColor, mother.eyeColor);
				hairColor.Inherit (father.hairColor, mother.hairColor);
			}
		}

		public override string ToString ()
		{
			return name;
		}

		public string FullName
		{
			get
			{
				return (isKing ? "King " : "") + name + " " + family.surname;
			}
		}

		string IJsonLinkable.Id { get { return name; } }
	}

	[JsonObject (MemberSerialization.OptIn)]
	public class Chance
	{
		[JsonProperty]
		public int CurrentChance { get; set; }

		public Chance (int min, int max)
		{
			CurrentChance = GenRandom.Next (min, max);
		}

		public Chance ()
		{

		}

		public bool Roll ()
		{
			int c = GenRandom.Next (100);

			return c < CurrentChance;
		}
	}
}
