﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace Family_Generator
{
	class Gedcom
	{

		static StringBuilder _builder;
		static List<GIndi> _individuals;
		static List<GFam> _families;

		public static string GetGedcom (GeneratorState state, string filePath)
		{
			_builder = new StringBuilder ();

			Append (0, "HEAD");

			WriteHeader (filePath);
			MatchIndis (state);
			WriteIndis ();
			WriteFams ();

			Append (0, "TRLR");

			return _builder.ToString ();
		}

		static void WriteHeader (string filePath)
		{
			Append (1, "SOUR", "Family Generator");
			Append (2, "NAME", "Family Generator");
			Append (2, "VERS", Program.VERSION);
			Append (2, "CORP", "RET World");
			Append (1, "DATE", ExportDate (DateTime.Now));
			Append (2, "TIME", DateTime.Now.ToLongTimeString ());
			Append (1, "FILE", Path.GetFileName (filePath));
			Append (1, "GEDC");
			Append (2, "VERS", "5.5");
			Append (2, "FORM", "LINEAGE-LINKED");
			Append (1, "CHAR", "UTF-8");
			Append (1, "LANG", "English");
		}

		static void WriteIndis ()
		{
			foreach (GIndi indi in _individuals)
			{
				Append (0, indi.id, "INDI");

				string name = indi.person.name;
				if (string.IsNullOrEmpty (indi.person.maidenName))
				{
					name += " /" + indi.person.family.surname + "/";
				}
				else
				{
					name += " /" + indi.person.maidenName + "/";
				}
				Append (1, "NAME", name);
				if (indi.person.isKing)
				{
					Append (2, "NPFX", "King");
				}
				Append (2, "GIVN", indi.person.name);
				Append (2, "SURN", indi.person.family.surname);
				Append (1, "SEX", indi.person.isFemale ? "F" : "M");
				Append (1, "CHAN");
				Append (2, "DATE", ExportDate (DateTime.Now));
				Append (1, "BIRT");
				Append (2, "DATE", GetDate (indi.person.yearOfBirth));

				if (indi.person.state == PersonState.Dead)
				{
					Append (1, "DEAT");
					Append (2, "CAUS", indi.person.causeOfDeath.ToString ());
					Append (2, "DATE", GetDate (indi.person.yearOfDeath));
				}

				if (indi.person.isKing)
				{
					//Append (1, "EVEN");
					//Append (2, "TYPE", "Coronation");
					//Append (2, "DATE", GetDate (indi.person.yearOfCrown));
					Append (1, "TITL", "King");
					Append (2, "DATE", GetDate (indi.person.yearOfCrown));
				}

				Append (1, "NOTE", "<b>Eye Color:</b> " + indi.person.eyeColor.color.ToString ());
				Append (2, "CONT", "<b>Hair Color:</b> " + indi.person.hairColor.color.ToString ());
				Append (2, "CONT", "<b>Personality:</b>");
				Append (2, "CONT", "<i>Openness:</i> " + indi.person.personality.openness.ToString (CultureInfo.InvariantCulture));
				Append (2, "CONT", "<i>Conscientiousness:</i> " + indi.person.personality.conscientiousness.ToString (CultureInfo.InvariantCulture));
				Append (2, "CONT", "<i>Extraversion:</i> " + indi.person.personality.extraversion.ToString (CultureInfo.InvariantCulture));
				Append (2, "CONT", "<i>Agreeableness:</i> " + indi.person.personality.agreeableness.ToString (CultureInfo.InvariantCulture));
				Append (2, "CONT", "<i>Neuroticism:</i> " + indi.person.personality.neuroticism.ToString (CultureInfo.InvariantCulture));

				if (indi.famc != null)
				{
					Append (1, "FAMC", indi.famc.id);
				}
				foreach (var f in indi.fams)
				{
					Append (1, "FAMS", f.id);
				}
			}
		}

		static void WriteFams ()
		{
			foreach (GFam fam in _families)
			{
				Append (0, fam.id, "FAM");
				Append (1, "HUSB", fam.husband.id);
				Append (1, "WIFE", fam.wife.id);

				foreach (var ch in fam.children)
				{
					Append (1, "CHIL", ch.id);
				}

				int spouseIndex = fam.husband.person.spouses.IndexOf (fam.wife.person);

				Append (1, "MARR");
				Append (2, "DATE", GetDate (fam.husband.person.yearsOfMarriage[spouseIndex]));
			}
		}

		static string GetDate (int date)
		{
			if (date > 0)
			{
				return date.ToString (CultureInfo.InvariantCulture);
			}

			return Math.Abs (date - 1).ToString (CultureInfo.InvariantCulture) + " BC";
		}

		static void MatchIndis (GeneratorState state)
		{
			_individuals = new List<GIndi> ();
			_families = new List<GFam> ();

			foreach (Family f in state.families)
			{
				foreach (Person p in f.persons)
				{
					GIndi indi = FindOrGenerateIndi (p);

					if (p.father != null && p.mother != null)
					{
						GFam fam = FindOrGenerateFam (p.mother, p.father);
						fam.children.Add (indi);
						indi.famc = fam;
					}

					if (p.spouses.Count > 0)
					{
						foreach (Person spouse in p.spouses)
						{
							if (p.isFemale)
							{
								FindOrGenerateFam (p, spouse);
							}
							else
							{
								FindOrGenerateFam (spouse, p);
							}
						}
					}
				}
			}
		}

		static GIndi FindOrGenerateIndi (Person p)
		{
			foreach (GIndi i in _individuals)
			{
				if (i.person == p)
				{
					return i;
				}
			}

			GIndi indi = new GIndi
			{
				id = "@I" + (_individuals.Count + 1) + "@",
				person = p
			};

			_individuals.Add (indi);

			return indi;
		}

		static GFam FindOrGenerateFam (Person mother, Person father)
		{
			GIndi iMother = FindOrGenerateIndi (mother);
			GIndi iFather = FindOrGenerateIndi (father);

			foreach (GFam f in _families)
			{
				if (f.wife == iMother && f.husband == iFather)
				{
					return f;
				}
			}

			GFam fam = new GFam ()
			{
				id = "@F" + (_families.Count + 1) + "@",
				husband = iFather,
				wife = iMother
			};

			iFather.fams.Add (fam);
			iMother.fams.Add (fam);

			_families.Add (fam);

			return fam;
		}

		static void Append (int h, string id, string val = "")
		{
			_builder.Append (h);
			_builder.Append (" ");
			_builder.Append (id);
			if (!string.IsNullOrEmpty (val))
			{
				_builder.Append (" ");
			}
			_builder.AppendLine (val);
		}

		static string ExportDate (DateTime? date)
		{
			if (date == null)
				return string.Empty;

			string day = date.Value.Day.ToString (CultureInfo.InvariantCulture);
			string year = date.Value.Year.ToString (CultureInfo.InvariantCulture);
			int month = date.Value.Month;

			string monthString = GetMMM (month);

			return day + " " + monthString + " " + year;
		}

		// ReSharper disable once InconsistentNaming
		static string GetMMM (int month)
		{
			string monthString = string.Empty;
			if (month == 1)
				monthString = "Jan";
			if (month == 2)
				monthString = "Feb";
			if (month == 3)
				monthString = "Mar";
			if (month == 4)
				monthString = "Apr";
			if (month == 5)
				monthString = "May";
			if (month == 6)
				monthString = "Jun";
			if (month == 7)
				monthString = "Jul";
			if (month == 8)
				monthString = "Aug";
			if (month == 9)
				monthString = "Sep";
			if (month == 10)
				monthString = "Oct";
			if (month == 11)
				monthString = "Nov";
			if (month == 12)
				monthString = "Dec";
			return monthString;
		}
	}

	class GIndi
	{
		public string id;
		public GFam famc;
		public List<GFam> fams = new List<GFam> ();

		public Person person;
	}

	class GFam
	{
		public string id;
		public GIndi wife;
		public GIndi husband;
		public List<GIndi> children = new List<GIndi> ();
	}
}
