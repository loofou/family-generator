﻿using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;
using System.Diagnostics;

namespace Family_Generator
{
	public class GenRandom
	{
		static readonly Random Rand = new Random ();

		public static int Next (int max)
		{
			return Rand.Next (max);
		}

		public static int Next (int min, int max)
		{
			return Rand.Next (min, max);
		}

		public static string RandomString (int size)
		{
			StringBuilder builder = new StringBuilder ();
			for (int i = 0; i < size; i++)
			{
				char ch = Convert.ToChar (Convert.ToInt32 (Math.Floor (26 * Rand.NextDouble () + 65)));
				builder.Append (ch);
			}

			return builder.ToString ();
		}

		public static string RandomName ()
		{
			string s = RandomWordGenerator.Word ();

			char[] a = s.ToCharArray ();
			a[0] = char.ToUpper (a[0]);
			return new string (a);
		}
	}

	public class ChanceList<T>
	{

		private readonly List<T> _pool = new List<T> ();

		public void AddToList (T obj, int weight)
		{
			for (int i = 0; i < weight; i++)
			{
				_pool.Add (obj);
			}
		}

		public void RemoveFromList (T obj)
		{
			while (_pool.Contains (obj))
			{
				_pool.Remove (obj);
			}
		}

		public void SetChanceFor (T obj, int weight)
		{
			RemoveFromList (obj);
			AddToList (obj, weight);
		}

		public T Roll ()
		{
			int c = GenRandom.Next (_pool.Count);

			_pool.Shuffle ();

			return _pool[c];
		}
	}

	public class Generator
	{
		public bool BREAK = false;

		public GeneratorState Process (GeneratorState startState)
		{
			GeneratorState state = CopyState (startState);

			state.currentYear++;

			//	Trace.WriteLine ("Processing next year: " + state.currentYear);

			for (int i = 0; i < state.families.Count; i++)
			{
				Family fam = state.families[i];
				fam.CleanUp ();

				if (fam.isFamilyDead)
					continue;

				//	Trace.WriteLine ("Processing Family: " + fam.surname);

				for (int j = 0; j < fam.persons.Count; j++)
				{
					Person person = fam.persons[j];

					if (person.state != PersonState.Dead)
					{
						//			Trace.WriteLine ("Processing Person: " + person.name);

						person.age = state.currentYear - person.yearOfBirth;

						CheckForDeath (state, person);

						CheckForMarriage (state, person);

						CheckForPregnancy (state, person);
					}
				}

				fam.CleanUp ();
			}

			state.CheckForEmptyFamilies ();
			state.CleanUp ();

			return state;
		}

		private void CheckForDeath (GeneratorState state, Person person)
		{
			if (person.chanceToDie.Roll ())
			{
				person.yearOfDeath = state.currentYear;
				person.state = PersonState.Dead;
				person.chanceToDie = null;

				ChanceList<CauseOfDeath> cause = new ChanceList<CauseOfDeath> ();

				if (person.age < 2)
				{
					cause.AddToList (CauseOfDeath.ChildDeath, 2 - person.age * 20);
				}
				if (person.age > 50)
				{
					cause.AddToList (CauseOfDeath.Decay, person.age);
				}
				if (person.age > 15 && person.age < 50)
				{
					cause.AddToList (CauseOfDeath.Battle, 15);
				}
				cause.AddToList (CauseOfDeath.Accident, 10);
				cause.AddToList (CauseOfDeath.Illness, 10);
				cause.AddToList (CauseOfDeath.Murder, 1);

				person.causeOfDeath = cause.Roll ();

				Trace.WriteLine (state.currentYear + " -> " + person.FullName + " dies of " + person.causeOfDeath.ToString () + "!");

				if (person.CurrentSpouse != null && person.CurrentSpouse.state == PersonState.Alive)
				{
					person.CurrentSpouse.marriage = MarriageStatus.Widowed;
					Trace.WriteLine (state.currentYear + " -> " + person.CurrentSpouse.FullName + " is widowed!");
				}

				if (person.isKing)
				{
					//Person successor = FindSuccessor (state, person);
					Person successor = FindSuccessor (state, state.families[0].persons[0]);
					if (successor != null)
					{
						Trace.WriteLine (state.currentYear + " -> " + successor.FullName + " is the new king!");
						successor.isKing = true;
						successor.yearOfCrown = state.currentYear;
					}
					else
					{
						Trace.WriteLine (state.currentYear + " -> No new king could be found!");
						BREAK = true;
					}
				}
			}
			else
			{
				int delta = GenRandom.Next (3);

				if (person.age < 15)
				{
					person.chanceToDie.CurrentChance = Math.Max (3, person.chanceToDie.CurrentChance - delta);
				}
				else if (person.isFemale ? person.age > 60 : person.age > 50)
				{
					person.chanceToDie.CurrentChance = Math.Min (100, person.chanceToDie.CurrentChance + delta);
				}
			}
		}

		private void CheckForMarriage (GeneratorState state, Person person)
		{
			if (person.age >= Person.MARRIAGEAGE && person.age < 50 && person.state == PersonState.Alive)
			{
				if (person.marriage == MarriageStatus.Single || person.marriage == MarriageStatus.Widowed)
				{
					if (person.chanceToMarry == null)
					{
						person.chanceToMarry = new Chance (10, 25);
					}

					if (person.chanceToMarry.Roll ())
					{
						Person spouse = FindSpouse (state, person);

						if (person.spouses == null)
							person.spouses = new List<Person> ();
						if (spouse.spouses == null)
							spouse.spouses = new List<Person> ();

						person.spouses.Add (spouse);
						spouse.spouses.Add (person);

						person.marriage = MarriageStatus.Married;
						spouse.marriage = MarriageStatus.Married;

						if (person.yearsOfMarriage == null)
							person.yearsOfMarriage = new List<int> ();
						if (spouse.yearsOfMarriage == null)
							spouse.yearsOfMarriage = new List<int> ();

						person.yearsOfMarriage.Add (state.currentYear);
						spouse.yearsOfMarriage.Add (state.currentYear);

						Trace.WriteLine (state.currentYear + " -> " + person.FullName + " and " + spouse.FullName + " have married!");

						bool canSwitch = state.settingFamilyChange == SettingFamilyChange.Both ||
										  (person.isFemale && state.settingFamilyChange == SettingFamilyChange.Female) ||
										  (!person.isFemale && state.settingFamilyChange == SettingFamilyChange.Male);

						if (canSwitch && person.chanceToSwitchFamily.Roll ())
						{
							Family fam1 = person.family;
							Family fam2 = spouse.family;
							fam1.MarkForDeletion (person);
							fam2.persons.Add (person);

							if (string.IsNullOrEmpty (person.maidenName))
							{
								person.maidenName = person.family.surname;
							}

							person.family = spouse.family;

							Trace.WriteLine (state.currentYear + " -> " + person.name + " joined Family " + spouse.family.surname);
						}
						else
						{
							Family fam1 = spouse.family;
							Family fam2 = person.family;
							fam1.MarkForDeletion (spouse);
							fam2.persons.Add (spouse);

							if (string.IsNullOrEmpty (spouse.maidenName))
							{
								spouse.maidenName = spouse.family.surname;
							}

							spouse.family = person.family;

							Trace.WriteLine (state.currentYear + " -> " + spouse.name + " joined Family " + person.family.surname);
						}

						person.chanceToMarry = null;
						spouse.chanceToMarry = null;
					}
				}
			}
		}

		private void CheckForPregnancy (GeneratorState state, Person person)
		{
			if (person.isFemale && person.marriage == MarriageStatus.Married && person.CurrentSpouse.state == PersonState.Alive && person.state == PersonState.Alive)
			{
				if (person.chanceToPregnancy == null && person.age <= 50 && person.CurrentSpouse.age <= 50)
				{
					person.chanceToPregnancy = new Chance (15, 35);
				}
				if (person.age > 50 || person.CurrentSpouse.age > 50)
				{
					person.chanceToPregnancy = null;
				}

				if (person.chanceToPregnancy != null)
				{
					if (person.yearOfLastChildbirth < state.currentYear - 1)
					{
						if (person.chanceToPregnancy.Roll ())
						{
							string name = GenRandom.RandomName ();
							while (CheckForNameCollision (state, name))
							{
								name = GenRandom.RandomName ();
							}

							Family family = person.family;
							Person child = new Person (family, name, state.currentYear, state.currentYear);

							family.persons.Add (child);

							child.father = person.CurrentSpouse;
							child.mother = person;

							child.isFemale = GenRandom.Next (100) < 50;
							child.chanceToSwitchFamily = new Chance (0, 33);
							child.InheritTraits ();

							person.children.Add (child);
							person.CurrentSpouse.children.Add (child);


							if (person.yearOfFirstChildbirth == 0)
							{
								person.yearOfFirstChildbirth = state.currentYear;
							}
							person.yearOfLastChildbirth = state.currentYear;

							Trace.WriteLine (state.currentYear + " -> " + person.FullName + " and " + person.CurrentSpouse.FullName + " got a child: " + child.FullName);
						}
					}
				}
			}
		}

		private Person FindSpouse (GeneratorState state, Person person)
		{
			foreach (Family fam in state.families)
			{
				foreach (Person p in fam.persons)
				{
					if (p.state == PersonState.Alive && p.age >= Person.MARRIAGEAGE && p.age < 50)
					{
						if (p.marriage == MarriageStatus.Single || p.marriage == MarriageStatus.Widowed)
						{
							if (p.family != person.family && p.isFemale != person.isFemale)
							{
								return p;
							}
						}
					}
				}
			}

			//No possible spouse in all families -> create new family with new person

			string name = GenRandom.RandomName ();
			while (CheckForNameCollision (state, name))
			{
				name = GenRandom.RandomName ();
			}

			Family family = new Family (name);
			state.families.Add (family);

			name = GenRandom.RandomName ();
			while (CheckForNameCollision (state, name))
			{
				name = GenRandom.RandomName ();
			}

			Person per = new Person (family, name, state.currentYear - GenRandom.Next (15, 30), state.currentYear + 1)
			{
				isFemale = !person.isFemale
			};

			per.InheritTraits ();

			family.persons.Add (per);

			Trace.WriteLine (state.currentYear + " -> " + "Generated new Family: " + family.surname + " and Person: " + per.name);

			return per;
		}

		private Person FindSuccessor (GeneratorState state, Person oldKing)
		{
			List<Person> descendants = new List<Person> ();

			foreach (var fam in state.families)
			{
				foreach (var person in fam.persons)
				{
					if (person.state == PersonState.Dead) continue;

					if ((person.isFemale && state.settingInheritThrone == SettingInheritThrone.OnlyMale) ||
						(!person.isFemale && state.settingInheritThrone == SettingInheritThrone.OnlyFemale)) continue;

					if (IsDescendantOf (person, oldKing))
					{
						descendants.Add (person);
					}
				}
			}

			descendants = descendants.OrderBy (o => o.yearOfBirth).ToList ();

			if (descendants.Count > 0)
				return descendants[0];

			return null;
		}

		private bool IsDescendantOf (Person person, Person ancestor)
		{
			if (person.father == null && person.mother == null)
				return false;

			if (person.father == ancestor || person.mother == ancestor)
				return true;

			if (IsDescendantOf (person.mother, ancestor))
				return true;

			return IsDescendantOf (person.father, ancestor);
		}

		public bool CheckForNameCollision (GeneratorState state, string name)
		{
			foreach (Family fam in state.families)
			{
				if (fam.surname == name)
					return true;

				foreach (Person p in fam.persons)
				{
					if (p.name == name)
						return true;
				}
			}

			return false;
		}

		public GeneratorState[] Process (GeneratorState startState, int steps, bool breakIfLastKingDies = false)
		{
			GeneratorState[] states = new GeneratorState[steps];

			GeneratorState lastState = startState;
			for (int i = 0; i < steps; i++)
			{
				states[i] = Process (lastState);
				lastState = states[i];

				if (BREAK && breakIfLastKingDies)
					break;
			}

			return states;
		}

		private GeneratorState CopyState (GeneratorState sourceState)
		{
			string c = JsonConvert.SerializeObject (sourceState, Program.settings);
			return (GeneratorState) JsonConvert.DeserializeObject (c, typeof (GeneratorState), Program.settings);
		}
	}

	static class Extensions
	{
		public static void Shuffle<T> (this IList<T> list)
		{
			int n = list.Count;
			while (n > 1)
			{
				n--;
				int k = GenRandom.Next (n + 1);
				T value = list[k];
				list[k] = list[n];
				list[n] = value;
			}
		}
	}
}

