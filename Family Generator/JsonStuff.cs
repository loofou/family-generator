﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Family_Generator
{
	public interface IJsonLinkable
	{
		string Id { get; }
	}

	public class JsonRefConverter : JsonConverter
	{
		public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
		{
			writer.WriteValue (((IJsonLinkable) value).Id);
		}

		public override object ReadJson (JsonReader reader, Type type, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType != JsonToken.String)
				throw new Exception ("Ref value must be a string.");
			return JsonLinkedContext.GetLinkedValue (serializer, type, reader.Value.ToString ());
		}

		public override bool CanConvert (Type type)
		{
			return type.IsAssignableFrom (typeof (IJsonLinkable));
		}
	}

	public class JsonRefedConverter : JsonConverter
	{
		public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
		{
			serializer.Serialize (writer, value);
		}

		public override object ReadJson (JsonReader reader, Type type, object existingValue, JsonSerializer serializer)
		{
			var jo = JObject.Load (reader);
			var value = JsonLinkedContext.GetLinkedValue (serializer, type, (string) jo.PropertyValues ().First ());
			serializer.Populate (jo.CreateReader (), value);
			return value;
		}

		public override bool CanConvert (Type type)
		{
			return type.IsAssignableFrom (typeof (IJsonLinkable));
		}
	}

	public class JsonLinkedContext
	{
		private readonly IDictionary<Type, IDictionary<string, object>> _links = new Dictionary<Type, IDictionary<string, object>> ();

		public static object GetLinkedValue (JsonSerializer serializer, Type type, string reference)
		{
			var context = (JsonLinkedContext) serializer.Context.Context;
			IDictionary<string, object> links;
			if (!context._links.TryGetValue (type, out links))
				context._links[type] = links = new Dictionary<string, object> ();
			object value;
			if (!links.TryGetValue (reference, out value))
				links[reference] = value = FormatterServices.GetUninitializedObject (type);
			return value;
		}
	}
}
