﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace Family_Generator
{
	static class Program
	{

		public const string VERSION = "1.0";

		private const string LogPath = "result/log.txt";

		static void Main (string[] args)
		{
			Trace.Listeners.Clear ();

			if (File.Exists (LogPath))
			{
				File.Delete (LogPath);
			}

			if (!Directory.Exists ("result"))
			{
				Directory.CreateDirectory ("result");
			}

			TextWriterTraceListener twtl = new TextWriterTraceListener (LogPath)
			{
				Name = "TextLogger",
				TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime
			};

			ConsoleTraceListener ctl = new ConsoleTraceListener (false) { TraceOutputOptions = TraceOptions.DateTime };

			Trace.Listeners.Add (twtl);
			Trace.Listeners.Add (ctl);
			Trace.AutoFlush = true;

			//Program

			Trace.WriteLine ("Family Generator\nv" + VERSION + "\nby LoofouEmu <loofou@ret-world.de>\n");

			if (args.Length == 0)
			{
				Trace.WriteLine (
					"You need to specify arguments to use this program!\n\nFamily Generator [sourcefile <-c stepcount -j -g -a -r>] or [-s]\n\n\t-j : generate json\n\t-g : generate gedcom\n\t-a : generate output for all steps\n\t-r : retry if king dies (needs a king to start with)\n\t-s : generate sample json");
			}
			else
			{
				if (args[0] == "-s")
				{
					CreateStartScript ();
				}
				else
				{
					string source = args[0];
					int steps = 1;
					bool json = false;
					bool gedcom = false;
					bool all = false;
					bool retry = false;

					bool c = false;
					for (int i = 1; i < args.Length; i++)
					{
						if (args[i] == "-c")
						{
							c = true;
						}
						else if (c)
						{
							int.TryParse (args[i], out steps);
							c = false;
						}
						else if (args[i] == "-j")
						{
							json = true;
						}
						else if (args[i] == "-g")
						{
							gedcom = true;
						}
						else if (args[i] == "-a")
						{
							all = true;
						}
						else if (args[i] == "-r")
						{
							retry = true;
						}
					}

					if (steps == 0)
						steps = 1;

					Trace.WriteLine ("Running Generator for " + steps + " steps for file: '" + source + "'\n");

					Generator generator = new Generator ();

					string start = File.ReadAllText (source);

					GeneratorState[] states;
					if (!retry)
					{
						GeneratorState state = (GeneratorState) JsonConvert.DeserializeObject (start, typeof (GeneratorState), settings);
						states = generator.Process (state, steps);
					}
					else
					{
						do
						{
							GeneratorState state = (GeneratorState) JsonConvert.DeserializeObject (start, typeof (GeneratorState), settings);
							generator.BREAK = false;
							states = generator.Process (state, steps, true);
							if (generator.BREAK)
								Trace.WriteLine ("\n---Retry---\n");
						} while (generator.BREAK);
					}

					if (json)
					{
						Trace.WriteLine ("\nGenerating JSON...");

						if (all)
						{
							foreach (GeneratorState s in states)
							{
								WriteJsonToFile (s);
							}
						}
						else
						{
							GeneratorState s = states[states.Length - 1];
							WriteJsonToFile (s);
						}
					}

					if (gedcom)
					{
						Trace.WriteLine ("\nGenerating GEDCOM...");

						if (all)
						{
							foreach (GeneratorState s in states)
							{
								WriteGedcomToFile (s);
							}
						}
						else
						{
							GeneratorState s = states[states.Length - 1];
							WriteGedcomToFile (s);
						}
					}
				}
			}

			Trace.WriteLine ("...Press Enter to Quit...");
			while (Console.ReadKey (true).Key != ConsoleKey.Enter) { }
		}

		static void WriteJsonToFile (GeneratorState state)
		{
			string json = JsonConvert.SerializeObject (state, Formatting.Indented, settings);
			File.WriteAllText ("result/state " + state.currentYear + ".json", json);
		}

		static void WriteGedcomToFile (GeneratorState state)
		{
			string file = "result/state " + state.currentYear + ".ged";
			string gedcom = Gedcom.GetGedcom (state, file);
			File.WriteAllText (file, gedcom);
		}

		static void CreateStartScript ()
		{
			Trace.WriteLine ("Generate Sample Script");

			GeneratorState state = new GeneratorState ();
			state.settingFamilyChange = SettingFamilyChange.Female;
			state.settingInheritThrone = SettingInheritThrone.OnlyMale;
			state.currentYear = 1;

			Family fam = new Family (GenRandom.RandomName ());
			Person per = new Person (fam, GenRandom.RandomName (), GenRandom.Next (-30, 1));

			fam.persons.Add (per);
			state.families.Add (fam);

			string json = JsonConvert.SerializeObject (state, Formatting.Indented, settings);
			File.WriteAllText ("sample.json", json);
		}

		public static JsonSerializerSettings settings = new JsonSerializerSettings
		{
			NullValueHandling = NullValueHandling.Ignore,
			Context = new StreamingContext (StreamingContextStates.All, new JsonLinkedContext ()),
			ObjectCreationHandling = ObjectCreationHandling.Replace,
			ReferenceLoopHandling = ReferenceLoopHandling.Serialize
		};
	}
}
