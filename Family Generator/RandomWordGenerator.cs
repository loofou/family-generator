﻿using System;
using System.Globalization;
using System.Linq;

namespace Family_Generator
{
	public class RandomWordGenerator
	{

		const int FirstSyllableHasStartConsonant = 60;
		const int SyllableHasStartConsonant = 50;
		const int SyllableHasEndConsonant = 70;
		const int StartConsonantComplex = 40;
		const int EndConsonantComplex = 50;
		const int FinalConsonantsModified = 30;
		const int VowelComplex = 30;

		static readonly int[] SyllableWeights = {
			2, // 1 syllable words
			5, // 2 syllable words
			3  // 3 syllable words
		};

		static readonly int CombinedSyllableWeights;

		static RandomWordGenerator ()
		{
			CombinedSyllableWeights = 0;
			foreach (int weight in SyllableWeights)
			{
				CombinedSyllableWeights += weight;
			}
		}

		static readonly char[] Vowels = {
			'a', 'e', 'i', 'o', 'u', 'y'
		};

		static readonly string[] ComplexVowels = {
			"ai", "au",
			"ea", "ee",
			"ie",
			"oo", "oa", "oi", "ou",
			"ua"					
		};

		static readonly string[] SimpleConsonants = {
			"b", "c", "d", "g", "l", "m", "n", "p", "s", "t", "w", "z", "v"
		};

		static readonly string[] StartConsonants = {
			"bl", "br",
			"ch", "cl", "cr",
			"dr",
			"f", "fl", "fr",
			"gl", "gr",
			"h",
			"j",
			"k", "kl", "kn", "kr",
			"pe", "ph", "pl", "pr",
			"qu",
			"rh",
			"sc", "sh", "sk", "sl", "sm", "sn", "sp", "st", "str",
			"th", "tr", "tw",
			"v",
			"wh", "wr",
			"y"
		};

		static readonly string[] EndConsonants = {
			"ch", "ck",
			"dge",
			"fe", "ff", "ft",
			"ght", "gh",
			"ke",
			"ld", "ll",
			"nd", "ng", "nk", "nt",
			"mg", "mp",
			"re", "rp", "rt",
			"sh", "sk", "sp", "ss", "st",
			"tch", "th",
			"x"
		};

		static readonly char[] EndConsonantsModifiable = {
			'b', 'd', 'g', 'l', 'm', 'n', 'r', 't'
		};

		static readonly char[] EndConsonantsMustBeModified = {
			'c', 'l', 'v', 's'
		};

		static readonly char[] ConsonantNeverFollows = {
		   'b'
		};

		// simple test - run through and
		public static void Test ()
		{
			for (int i = 0; i < 30; i++)
			{
				Console.WriteLine (Word ());
			}
		}

		public static string Word ()
		{
			// determine no of syllables
			int syllables = 0;
			var selector = GenRandom.Next (CombinedSyllableWeights - 1);
			for (var i = 0; i < SyllableWeights.Length; i++)
			{
				var weight = SyllableWeights[i];
				if (selector < weight)
				{
					syllables = i + 1;
					break;
				}
				selector -= weight;
			}
			return Word (syllables);
		}

		public static string Word (int syllables)
		{
			if (syllables < 1) throw new ArgumentException ("Word must have at least 1 syllable.");
			string word = "";
			string lastSyllable = null;
			for (int i = 0; i < syllables; i++)
			{
				bool makeItShort = i > 0 && syllables > 2;
				bool last = i == syllables - 1;
				string syllable = Syllable (lastSyllable, makeItShort, last);

				// single syllable words must be more than one character
				while (syllables == 1 && syllable.Length == 1)
				{
					syllable = Syllable (lastSyllable, makeItShort, last);
				}

				//if(i > 0) word += "-";
				word += syllable;
				lastSyllable = syllable;
			}

			return word;
		}

		static string Syllable (string previous, bool makeItShort, bool isLast)
		{
			bool isFirst = previous == null;
			bool lastSyllableSimple = previous != null && previous.Length < 3;
			bool lastSyllableComplexEnd = false;
			bool lastLetterIsNeverFollowedByConsonant = false;

			if (previous != null)
			{
				int count = 0;
				for (int i = previous.Length - 1; i > 0; i--)
				{
					char lookAt = previous[i];
					if (!Vowels.Contains (lookAt)) count++;
					else break;
				}
				lastSyllableComplexEnd = count > 1;

				if (ConsonantNeverFollows.Contains (previous[previous.Length - 1])) lastLetterIsNeverFollowedByConsonant = true;
			}

			bool startConsonant = isFirst ?
				GenRandom.Next (100) < FirstSyllableHasStartConsonant : GenRandom.Next (100) < SyllableHasStartConsonant;

			startConsonant = startConsonant && (isFirst || lastLetterIsNeverFollowedByConsonant);

			// always start with consonant if previous syllable ends in vowel
			if (!startConsonant && previous != null && !lastLetterIsNeverFollowedByConsonant)
			{
				char lastChar = previous[previous.Length - 1];
				if (Vowels.Contains (lastChar)) startConsonant = true;
			}

			bool endConsonant = !(makeItShort && startConsonant) && GenRandom.Next (100) < SyllableHasEndConsonant;

			// always end in consonant if the last syllable was short
			if (lastSyllableSimple && !endConsonant)
			{
				endConsonant = true;
			}

			string syllable = "";

			string start = null;
			bool complexStartConsonant = false;
			if (startConsonant)
			{
				complexStartConsonant = !lastSyllableComplexEnd && GenRandom.Next (100) < StartConsonantComplex;
				if (complexStartConsonant) start = StartConsonants.Random ();
				else start = SimpleConsonants.Random ();
				syllable += start;
			}

			bool complexVowel = startConsonant && !complexStartConsonant && GenRandom.Next (100) < VowelComplex;
			if (complexVowel)
				syllable += ComplexVowels.Random ();
			else
				syllable += Vowels.Random ().ToString (CultureInfo.InvariantCulture);

			string end;
			if (endConsonant)
			{
				bool complex = GenRandom.Next (100) < EndConsonantComplex;
				bool startEndsWithR = start != null && start.EndsWith ("r");
				do
				{
					if (complex) end = EndConsonants.Random ();
					else end = SimpleConsonants.Random ();
				} while (startEndsWithR && end.StartsWith ("r"));

				bool modify = isLast && (EndConsonantsMustBeModified.Contains (end[end.Length - 1]) || (
					(end == "ch" || EndConsonantsModifiable.Contains (end[end.Length - 1])) && GenRandom.Next (100) < FinalConsonantsModified));
				if (modify)
					end += "e";

				syllable += end;
			}

			return syllable;
		}
	}

	static class RandomWordExtensions
	{

		public static string Random (this string[] source)
		{
			return source[GenRandom.Next (source.Length - 1)];
		}

		public static char Random (this char[] source)
		{
			return source[GenRandom.Next (source.Length - 1)];
		}
	}
}

