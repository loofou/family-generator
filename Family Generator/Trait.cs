﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Family_Generator
{

	[JsonObject (MemberSerialization.OptIn)]
	public interface ITrait
	{
		void Create ();
		void Inherit (ITrait fatherTrait, ITrait motherTrait);
	}

	[JsonObject (MemberSerialization.OptIn)]
	public class PersonalityTrait : ITrait
	{
		[JsonProperty]
		public int openness;
		[JsonProperty]
		public int conscientiousness;
		[JsonProperty]
		public int extraversion;
		[JsonProperty]
		public int agreeableness;
		[JsonProperty]
		public int neuroticism;

		public void Create ()
		{
			openness = GenRandom.Next (10);
			conscientiousness = GenRandom.Next (10);
			extraversion = GenRandom.Next (10);
			agreeableness = GenRandom.Next (10);
			neuroticism = GenRandom.Next (10);
		}

		public void Inherit (ITrait fatherTrait, ITrait motherTrait)
		{
			PersonalityTrait father = (PersonalityTrait) fatherTrait;
			PersonalityTrait mother = (PersonalityTrait) motherTrait;

			openness = Math.Min (9, Math.Max (0, (father.openness + mother.openness) / 2 + GenRandom.Next (-5, 6)));
			conscientiousness = Math.Min (9, Math.Max (0, (father.conscientiousness + mother.conscientiousness) / 2 + GenRandom.Next (-5, 6)));
			extraversion = Math.Min (9, Math.Max (0, (father.extraversion + mother.extraversion) / 2 + GenRandom.Next (-5, 6)));
			agreeableness = Math.Min (9, Math.Max (0, (father.agreeableness + mother.agreeableness) / 2 + GenRandom.Next (-5, 6)));
			neuroticism = Math.Min (9, Math.Max (0, (father.neuroticism + mother.neuroticism) / 2 + GenRandom.Next (-5, 6)));
		}
	}

	[JsonObject (MemberSerialization.OptIn)]
	public class EyeColorTrait : ITrait
	{
		public enum EyeColor
		{
			Brown,
			Blue,
			Green
		}

		public enum EyeColorGeneBrown
		{
			Brown,
			NotBrown
		}

		public enum EyeColorGeneBlue
		{
			Blue,
			Green
		}

		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public EyeColorGeneBlue blueAllel1;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public EyeColorGeneBrown brownAllel1;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public EyeColorGeneBlue blueAllel2;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public EyeColorGeneBrown brownAllel2;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public EyeColor color;

		public void Create ()
		{
			blueAllel1 = GenRandom.Next (100) < 50 ? EyeColorGeneBlue.Blue : EyeColorGeneBlue.Green;
			brownAllel1 = GenRandom.Next (100) < 50 ? EyeColorGeneBrown.Brown : EyeColorGeneBrown.NotBrown;
			blueAllel2 = GenRandom.Next (100) < 50 ? EyeColorGeneBlue.Blue : EyeColorGeneBlue.Green;
			brownAllel2 = GenRandom.Next (100) < 50 ? EyeColorGeneBrown.Brown : EyeColorGeneBrown.NotBrown;

			CalculateGenes ();
		}

		public void Inherit (ITrait fatherTrait, ITrait motherTrait)
		{
			EyeColorTrait father = (EyeColorTrait) fatherTrait;
			EyeColorTrait mother = (EyeColorTrait) motherTrait;

			blueAllel1 = GenRandom.Next (100) < 50 ? father.blueAllel1 : father.blueAllel2;
			blueAllel2 = GenRandom.Next (100) < 50 ? mother.blueAllel1 : mother.blueAllel2;
			brownAllel1 = GenRandom.Next (100) < 50 ? father.brownAllel1 : father.brownAllel2;
			brownAllel2 = GenRandom.Next (100) < 50 ? mother.brownAllel1 : mother.brownAllel2;

			CalculateGenes ();
		}

		void CalculateGenes ()
		{
			if (brownAllel1 == EyeColorGeneBrown.Brown || brownAllel2 == EyeColorGeneBrown.Brown)
			{
				color = EyeColor.Brown;
			}
			else
			{
				if (blueAllel1 == EyeColorGeneBlue.Blue || blueAllel2 == EyeColorGeneBlue.Blue)
				{
					color = EyeColor.Blue;
				}
				else
				{
					color = EyeColor.Green;
				}
			}
		}
	}

	[JsonObject (MemberSerialization.OptIn)]
	public class HairColorTrait : ITrait
	{
		public enum HairColor
		{
			Black,
			Brown,
			Red,
			Strawblonde,
			Blonde
		}

		public enum HairColorGeneBrown
		{
			Brown,
			NotBrown
		}

		public enum HairColorGeneRed
		{
			Red,
			NotRed
		}

		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public HairColorGeneRed redAllel1;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public HairColorGeneBrown brownAllel1;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public HairColorGeneRed redAllel2;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public HairColorGeneBrown brownAllel2;
		[JsonProperty, JsonConverter (typeof (StringEnumConverter))]
		public HairColor color;

		public void Create ()
		{
			redAllel1 = GenRandom.Next (100) < 50 ? HairColorGeneRed.Red : HairColorGeneRed.NotRed;
			brownAllel1 = GenRandom.Next (100) < 50 ? HairColorGeneBrown.Brown : HairColorGeneBrown.NotBrown;
			redAllel2 = GenRandom.Next (100) < 50 ? HairColorGeneRed.Red : HairColorGeneRed.NotRed;
			brownAllel2 = GenRandom.Next (100) < 50 ? HairColorGeneBrown.Brown : HairColorGeneBrown.NotBrown;

			CalculateGenes ();
		}

		public void Inherit (ITrait fatherTrait, ITrait motherTrait)
		{
			HairColorTrait father = (HairColorTrait) fatherTrait;
			HairColorTrait mother = (HairColorTrait) motherTrait;

			redAllel1 = GenRandom.Next (100) < 50 ? father.redAllel1 : father.redAllel2;
			redAllel2 = GenRandom.Next (100) < 50 ? mother.redAllel1 : mother.redAllel2;
			brownAllel1 = GenRandom.Next (100) < 50 ? father.brownAllel1 : father.brownAllel2;
			brownAllel2 = GenRandom.Next (100) < 50 ? mother.brownAllel1 : mother.brownAllel2;

			CalculateGenes ();
		}

		void CalculateGenes ()
		{
			if (brownAllel1 == HairColorGeneBrown.Brown && brownAllel2 == HairColorGeneBrown.Brown &&
				redAllel1 == HairColorGeneRed.Red && redAllel2 == HairColorGeneRed.Red)
			{
				color = HairColor.Black;
			}
			else if (brownAllel1 == HairColorGeneBrown.NotBrown && brownAllel2 == HairColorGeneBrown.NotBrown &&
			  redAllel1 == HairColorGeneRed.NotRed && redAllel2 == HairColorGeneRed.NotRed)
			{
				color = HairColor.Blonde;
			}
			else if (brownAllel1 != brownAllel2 && redAllel1 != redAllel2)
			{
				color = HairColor.Strawblonde;
			}
			else if (redAllel1 == HairColorGeneRed.Red && redAllel2 == HairColorGeneRed.Red)
			{
				color = HairColor.Red;
			}
			else
			{
				color = HairColor.Brown;
			}
		}
	}
}
