Family Generator
v1.0
by @LoofouEmu <loofou@ret-world.de>
Licensed under GPL v3

1. What is Family Generator?

This Tool is for generating random family trees.
You can use it to create families for your role playing game or just for fun.

It exports to json files, which are human readable and easy to edit.
A GUI could follow in the future.

2. How to use

Download the bundled version or compile from source.

Family Generator is a command line program, so you need to open your command promt (Win+R -> "cmd").

There are two possible ways to generate files with this tool:

	1. Generating a sample start json. You need to do this at least once.
		-> Run "Family Generator.exe -s"
	
	2. Generating a family tree for a period of time.
		-> Run "Family Generator.exe start.json"
		You can also add the following parameters:
			"-c #": generate a custom number of years (ex. "-c 500" for 500 years)
			"-j": export to json (you would want to add this)
			"-g": export to gedcom (for viewing in software that can import gedcom files)
			"-a": export all states of every generated year.

		The best command to get things done fast would be: 
			"Family Generator.exe start.json -c 500 -j -g"

3. Gedcom files

Gedcom is a common file format for genealogy data. It can be imported by a large number of programs.
I recommend reading the wikipedia article: http://en.wikipedia.org/wiki/GEDCOM.
This is a list of programs that can import or view gedcom files: http://www.cyndislist.com/gedcom/gedcom-software/

If you have any questions, just send me a mail or follow me on twitter @LoofouEmu.
